const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bankSchema = new Schema(
  {
    name: { type: String, required: true },
    userId: { type: String, required: false },
    image: { type: String },
    initialBalance: { type: Number, required: false },
    income: [{ type: mongoose.Types.ObjectId, ref: 'Incomes' }],
    expense: [{ type: mongoose.Types.ObjectId, ref: 'Expenses' }],
  },
  {
    timestamps: true,
  }
);

const Bank = mongoose.model('Banks', bankSchema);

module.exports = Bank;
