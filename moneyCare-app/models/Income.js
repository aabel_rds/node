const mongoose = require(`mongoose`);

const Schema = mongoose.Schema;

const incomeSchema = new Schema(
  {
    amount: { type: Number, required: true },
    date: { type: Date, required: true },
    type: { type: String, required: true },
    Description: { type: String, required: false },
    bankReceipt: { type: String, required: false },
    userID: { type: String, required: false },
  },
  {
    timestamps: true,
  }
);

const Income = mongoose.model('Incomes', incomeSchema);

module.exports = Income;
