const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const expenseSchema = new Schema(
  {
    amount: { type: Number, required: true },
    date: { type: Date, required: true },
    type: { type: String, required: true },
    Description: { type: String, required: false },
    bankReceipt: { type: String, required: false },
    userID: { type: String, required: false },
  },
  {
    timestamps: true,
  }
);

const Expense = mongoose.model('Expenses', expenseSchema);

module.exports = Expense;
