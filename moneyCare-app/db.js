const mongoose = require('mongoose');
require('dotenv').config();
const DB_URL = process.env.DB_URL;

mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Connected to the dataBase');
  })
  .catch((err) => {
    console.log(err || 'Unexpected error when trying to connect to the dataBase');
  });

module.exports = DB_URL;
