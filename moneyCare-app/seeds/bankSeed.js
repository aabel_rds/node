require('dotenv').config();
const mongoose = require('mongoose');

const Banks = require('../models/Bank');
const DB_URL = 'mongodb://localhost:27017/moneyCareDB';

const banks = [
  {
    userId: '',
    name: 'BBVA',
    image: 'https://www.bbva.com/wp-content/uploads/2019/06/Marqueex2.png',
    initialBalance: 0,
  },
  {
    userId: '',
    name: 'SABADELL',
    image: 'https://www.andaluciaemprende.es/wp-content/uploads/2016/01/logoSabadell.png',
    initialBalance: 0,
  },
  {
    userId: '',
    name: 'SANTANDER',
    image: 'https://brandemia.org/sites/default/files/santander_logo_copia.jpg',
    initialBalance: 0,
  },
  {
    userId: '',
    name: 'CAIXA',
    image: 'https://www.caixabank.com/docs/comunicacion/60480.jpg',
    initialBalance: 0,
  },
  {
    userId: '',
    name: 'ING',
    image:
      'https://www.telefonoatencionclientes.com/wp-content/uploads/2017/12/telefono-ing-direct.jpg',
    initialBalance: 0,
  },
];
const bankDocuments = banks.map((bank) => new Banks(bank));
mongoose
  .connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allBanks = await Banks.find();
    if (allBanks.length) {
      await Banks.collection.drop();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))

  .then(async () => Banks.insertMany(bankDocuments))

  .catch((err) => console.log(`Error creating data ${err}`))

  .finally(() => mongoose.disconnect());
