const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const path = require('path');
const indexRouter = require('./routes/index.routes');
const userRouter = require('./routes/user.routes');
const bankRouter = require('./routes/bank.routes');
const expenseRouter = require('./routes/expense.routes');
const incomeRouter = require('./routes/income.routes');
require('dotenv').config();
require(`./db`);
require('./passport');
const PORT = process.env.PORT || 5000;
const server = express();
// setting the view path and hbs view engine
server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

// To handle the resquest and transform the request sent to the server in json format and we obtain it in the req.body
server.use(express.json());
server.use(
  express.urlencoded({
    extended: false,
  })
);

// We tell the server which is the public directory
server.use(express.static(path.join(__dirname, 'public')));

// configuring the user session and the cookie
server.use(
  session({
    secret: 'moneyCareOrdnajela',
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 72000000,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
);

// Passport (User and sessions users controller)
server.use(passport.initialize());
server.use(passport.session());

// Routes
server.use('/', indexRouter);
server.use('/user', userRouter);
server.use('/bank', bankRouter);
server.use('/expense', expenseRouter);
server.use('/income', incomeRouter);

// Middelware, controlling routes errors (express)
server.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  res.status(404);
  next(error);
});

// Controlling routes errors using express and the 4th parameter error
server.use((err, req, res, next) => {
  return res.status(err.status || 500).render('error', {
    message: err.message || 'Unexpected error',
    status: err.status || 500,
  });
});

// Server started
server.listen(PORT, () => {
  console.log(`Server Listening in http://localhost:${PORT}`);
});
