const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const Income = require('../models/Income');
const Bank = require('../models/Bank');

router.get('/create', async (req, res, next) => {
  try {
    const userId = req.user._id;
    const userBanks = await Bank.find({ userId: userId });

    return res.render('createIncome', { message: 'Income', userBanks });
  } catch (error) {
    return res.render('error', { error });
  }
});

router.post('/create', async (req, res, next) => {
  try {
    const userID = req.user._id;
    if (!userID) {
      return res.redirect('/login');
    }
    const { amount, date, incomeType, description, bankReceipt, bank } = req.body;

    const userBanks = await Bank.find({ userId: userID, name: bank });
    const bankId = userBanks.map((userBank) => {
      if (userBank.name == bank) {
        return userBank._id;
      }
    });

    const fixed = parseFloat(amount).toFixed(2);

    const newIncome = new Income({
      amount: fixed,
      date,
      type: incomeType,
      description,
      bankReceipt,
      userID,
      bank,
    });

    const saveIncome = await newIncome.save();
    const bankIncome = await Bank.findByIdAndUpdate(
      bankId,
      { $push: { income: saveIncome._id } },
      { new: true }
    );

    return res.render('createExpense', { created: 'Created!', saveIncome, userBanks, bankIncome });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
