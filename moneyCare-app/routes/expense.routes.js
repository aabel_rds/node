const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const Expense = require('../models/Expense');
const Bank = require('../models/Bank');
const fileMiddlewares = require('../middlewares/file.middleware');

router.get('/create', async (req, res, next) => {
  try {
    const userId = req.user._id;
    const userBanks = await Bank.find({ userId: userId });

    return res.render('createExpense', { message: 'Expense', userBanks });
  } catch (error) {
    return res.render('error', { error });
  }
});

router.post('/create', [fileMiddlewares.single('bankReceipt')], async (req, res, next) => {
  try {
    console.log(req.file);
    const userID = req.user._id;
    const expensePicture = req.file ? req.file.filename : null;

    if (!userID) {
      return res.redirect('/login');
    }
    const { amount, date, expenseType, description, bank } = req.body;

    const userBanks = await Bank.find({ userId: userID, name: bank });
    const bankId = userBanks.map((userBank) => {
      if (userBank.name == bank) {
        return userBank._id;
      }
    });

    const fixed = parseFloat(amount).toFixed(2);

    const newExpense = new Expense({
      amount: fixed,
      date,
      type: expenseType,
      description,
      bankReceipt: expensePicture,
      userID,
      bank,
    });

    const saveExpense = await newExpense.save();
    const bankExpense = await Bank.findByIdAndUpdate(
      bankId,
      { $push: { expense: saveExpense._id } },
      { new: true }
    );

    return res.render('createExpense', {
      message: 'Created!',
      saveExpense,
      userBanks,
      bankExpense,
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
