const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const Bank = require('../models/Bank');
const User = require('../models/User');

const add = 'ADD A BANK';

router.get('/create', (req, res, next) => {
  const user = req.user;
  if (!user) {
    return res.redirect('/login');
  }
  return res.status(200).render('createBank', { add });
});

router.post('/create', async (req, res, next) => {
  try {
    const { bank, ibalance } = req.body;
    const userId = req.user._id;
    const bankExist = await Bank.findOne({ userId: userId, name: bank });

    if (bankExist) {
      const error = new Error('The Bank already exist');
      console.log(bankExist);
      return res.render('createBank', { error: error, bank: bankExist });
    }

    const bankSearch = await Bank.findOne({ name: bank });
    console.log('en el create', bankSearch);
    const newBank = new Bank({
      userId,
      name: bank,
      image: bankSearch.image,
      initialBalance: ibalance,
    });

    const saveBank = await newBank.save();
    const userBank = await User.findByIdAndUpdate(
      userId,
      { $push: { banks: saveBank._id } },
      { new: true }
    );

    res.status(200).render('createBank', { saveBank, userBank, message: 'Created' });
  } catch (error) {
    next(error);
  }
});

router.get('/user/:id', async (req, res, next) => {
  try {
    const id = req.params.id;

    const incomeDetail = await Bank.findById(id).populate('income');
    const expenseDetail = await Bank.findById(id).populate('expense');
    const incomes = incomeDetail.income;
    const expenses = expenseDetail.expense;
    const balance = expenseDetail.initialBalance;
    console.log(balance);
    return res.status(200).json({ incomes, expenses, balance });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
