const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/', (req, res, next) => {
  try {
    const user = req.user;
    if (!user) {
      res
        .status(200)
        .render('index', { titlle: 'MoneyCare', titleDescription: 'welcome to the MoneyCareApp' });
    } else {
      res.redirect('/user');
    }
  } catch (error) {
    next(error);
  }
});

router.get('/register', (req, res, next) => {
  return res.status(200).render('register', { title: 'User Register' });
});

router.get('/login', (req, res, next) => {
  return res.status(200).render('login', { title: 'LogIn' });
});

module.exports = router;
