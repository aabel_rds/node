const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const passport = require('passport');
const User = require('../models/User');
require('../passport');

router.get('/', async (req, res, next) => {
  try {
    const user = req.user;

    if (!user) {
      return res.redirect('login');
    } else {
      const { email, image, banks } = req.user;

      const userBanks = await User.findOne({ banks }).populate('banks');
      console.log('En usuario', userBanks.banks);
      return res.render('user', {
        name: email,
        image,
        userBanks: userBanks.banks,
      });
    }
  } catch (error) {
    next(error);
  }
});

router.post('/register', (req, res, next) => {
  // we receive error and user from the middleware register from passport.js
  passport.authenticate('register', (error, user) => {
    if (error) {
      return res.render('register', { error: error.message });
    }

    req.logIn(user, (error) => {
      if (error) {
        return res.render('register', { error: error.message });
      }

      return res.redirect('/user');
    });
  })(req, res, next);
});

router.post('/login', (req, res, next) => {
  // we receive error and user from the middleware register from passport.js
  passport.authenticate('login', (error, user) => {
    if (error) {
      return res.render('login', { error: error.message });
    }

    req.logIn(user, (error) => {
      if (error) {
        return res.render('login', { error: error.message });
      }
      return res.redirect(`/user/`);
    });
  })(req, res, next);
});

router.post('/logout', (req, res, next) => {
  if (req.user) {
    // destroy the req.user
    req.logOut();
    // destroy and clear the session cookie
    req.session.destroy(() => {
      res.clearCookie('connect.sid');
      return res.redirect('/');
    });
  } else {
    return res.sendStatus(304);
  }
});

module.exports = router;
