// Middleware, passport handle the authentications processes
const paspport = require('passport');
// passport-local, handle the authentication estrategies with email and password
const LocalStrategy = require('passport-local').Strategy;
// bcrypt encrypts the users passwords
const bcrypt = require('bcrypt');

const User = require('./models/User');
// salt is the encrypt level
const saltRounds = 10;

const validate = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
  // the password must have any of following characters (. , - _ )
  const pass = /\S+[\.\,\-\_]|[\.\,\-\_]+\S/;

  if (password.length < 8) {
    return false;
  }

  return pass.test(String(password));
};

paspport.use(
  'register',
  new LocalStrategy(
    {
      usernameField: 'email', // we choose the email field
      passwordField: 'password', // we choose the password field
      passReqToCallback: true, // makes the callback receive the request
    },
    async (req, email, password, done) => {
      // the register code

      try {
        const userExist = await User.findOne({ email: email.toLowerCase() });
        const validEmail = validate(email);
        const validPass = validatePass(password);

        if (!validEmail) {
          const error = new Error(`Invalid email`);

          return done(error);
        }
        if (!validPass) {
          // console.log('1 validador', validEmail);
          const error = new Error(
            `The password must be greater than 8 and have any of following characters ( "," "." "-" "_" ) `
          );

          return done(error);
        }

        if (userExist) {
          const error = new Error(`The user ${email} is already registered`);
          return done(error);
        }

        // password hash receives two parameters, password and the encrypt level
        const hash = await bcrypt.hash(password, saltRounds);
        // save the user
        const newUser = new User({
          email,
          password: hash,
          image: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png',
        });
        const saveUser = await newUser.save();

        return done(null, saveUser);
      } catch (error) {
        return done(error);
      }
    }
  )
);

paspport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email', // we choose the email field
      passwordField: 'password', // we choose the password field
      passReqToCallback: true, // makes the callback receive the request
    },
    async (req, email, password, done) => {
      // the login code

      try {
        const userExist = await User.findOne({ email: email.toLowerCase() });
        const validEmail = validate(email);
        const isValidPass = await bcrypt.compare(password, userExist.password);
        const validPass = validatePass(password);

        if (!validEmail) {
          const error = new Error(`Invalid email`);

          return done(error);
        }
        if (!validPass) {
          // console.log('1 validador', validEmail);
          const error = new Error(`Invalid password or email `);

          return done(error);
        }

        if (!userExist) {
          const error = new Error(`The user ${email} does not exist!`);
          return done(error);
        }

        if (!isValidPass) {
          const error = new Error(`The email or password are invalid`);
          return done(error);
        }

        return done(null, userExist);
      } catch (error) {
        return done(error);
      }
    }
  )
);

// save the userID in the req.user
paspport.serializeUser((user, done) => {
  return done(null, user._id);
});
// search the user in to the db and save in to the req.user
paspport.deserializeUser(async (user, done) => {
  try {
    const findUser = await User.findById(user);
    return done(null, findUser);
  } catch (error) {
    return done(error);
  }
});
