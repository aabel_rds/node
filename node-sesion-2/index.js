const express = require('express');
const fs = require('fs');
const { parse } = require('path');
const PORT = 5000;
const server = express();
const router = express.Router();

router.get('/', (req, res) => {
    res.send('HELLO WORLD');
});

router.get('/movies/', ( req, res ) => {


    const movieName = req.query.name;
    const newMovie = [];

    const moviesRequest = fs.readFile('movies.json', ( err,data )=>{

        if(err){
            console.log('there was an error reading the file')
        }

        const parseData = JSON.parse(data);

        if(!movieName){

            res.send(parseData);
        }

        if(movieName){

           const finder =  parseData.find((movie) =>{
            
                if(movie.name == movieName){
                    
                    return res.send(movie);
                };

            });
                if(finder === undefined){

                    const moviesLength = parseData.length + 1;
                    console.log(moviesLength);
                    parseData.push({"id": moviesLength, "name":  movieName });
                    console.log(parseData);
                    const newMovies = JSON.stringify(parseData);
                    fs.writeFile('movies.json', newMovies, () => res.send('Hemos añadido tu pelicula') );
                }

        }
    });



})

router.get('/movies/:id',(req, res) => {
    const movieID = req.params.id;
    console.log(movieID);

    const movieRequest = fs.readFile('movies.json', ( err,data )=>{
        if(err){
            console.log('there was an error reading the file')
        }
        const parseData = JSON.parse(data);

        parseData.map((movie)=>{

            if(movie.id == movieID){
                
                res.send(movie);
            }
            else{

                res.status(404);
            }
        })

    });
    
})




server.use('/', router);

server.listen(PORT, () => {
    console.log(`SERVER LISTENING ON http://localhost:${PORT}`)
});
