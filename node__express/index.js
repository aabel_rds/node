const express = require('express');
const fs = require('fs');
const { parse } = require('path');
const { send } = require('process');

const PORT = 5000;
const server = express();

const router = express.Router();

router.get('/movies', (req,res) => {

    const name = req.query.name;

    fs.readFile('movies.json', (err, data) => {

        if(err){
            console.log('Error reading file',err);
        } else{
            
            const parseData = JSON.parse(data);

            if(name){

                const result = parseData.find(movie => movie.name.toLowerCase() === name.toLocaleLowerCase());

                if(result){
                    res.send(result);
                }else{
                    let newLength = parseData.length + 1;
                    let newMovie = {
                        id: newLength,
                        name : name
                    }
                    console.log(newLength);
                    parseData.push(newMovie)
                    console.log(parseData);


                    fs.writeFile('movies.json', JSON.stringify(parseData), () => {
                        console.log('Write file success !!');
                    });

                    res.send(`No teníamos la película ${newMovie.name}, pero la hemos añadido `);
                }
            }else{
                res.send(parseData);
            }

        }
    })
})

router.get('/movies/:id',(req, res)=>{
    const id = Number(req.params.id);

    fs.readFile('movies.json', (err, data) => {

        if(err){
            console.log('Error reading file',err);

        } else{
            
            const parseData = JSON.parse(data);

            parseData.forEach(element => {

                if(element.id == id){

                    res.send(element);
                }
                
            });
        }
    })
})

server.use('/',router);
server.listen(PORT, () =>{
    console.log(`server running in http://localhost:${PORT}`);

})



