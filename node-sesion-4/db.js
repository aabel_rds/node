const mongoose = require('mongoose');

const URL_DB = 'mongodb://localhost:27017/node-sesion3';

mongoose.connect(URL_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then( () => {

    console.log(`Succsess connected to the DB`);
})
.catch((err) => {
    console.log('Error connecting to Data base');
})

module.exports = URL_DB;