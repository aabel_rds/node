const mongoose = require('mongoose');
const URL_DB = require('../db');
const Pet = require('../models/Pet');
// create the pets of the seed
const pets = [
    {
      name: 'Curro',
      age: 3,
      species: 'dog',
    },
    {
      name: 'Nala',
      age: 2,
      species: 'cat',
    },
    {
      name: 'Margarita',
      age: 6,
      species: 'dog',
    },
    {
      name: 'Simón',
      age: 8,
      species: 'turtle',
    },
    {
      name: 'Max',
      age: 5,
      species: 'dog',
    },
  ];
// 
//   const petDocuments = pets.map( pet => new Pet(pet));

  mongoose.connect( URL_DB, {
      useNewUrlParser:true,
      useUnifiedTopology:true
  })
  .then( async () => {

      const allPets = await Pet.find();

      if( allPets.length ){

        await Pet.collection.drop();
      }
  })
.catch((err) => console.log(`Error deleting data ${err}`))

.then( async () => {
    await Pet.insertMany(pets);
})
.catch((err) => console.log(`Error creating data: ${err}`))

.finally(() => mongoose.disconnect());
