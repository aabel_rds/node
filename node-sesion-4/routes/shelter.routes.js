const express = require('express');

const Shelter = require('../models/Shelter');

const router = express.Router();

router.post('/create', async ( req, res, next ) => {

    try {

        const newShelter = new Shelter ({

            name: req.body.name,
            location: req.body.location
        });

        const createdShelter = await newShelter.save();

        return res.status(200).json(createdShelter);

    } catch (error) {

        next(error);
    }

});

// Update route shelter to add pets requiring petid and shelterId
router.put('/add-pet', async ( req, res, next) => {


    try {

        const petID = req.body.petID;
        const shelterId = req.body.shelterId;

        const addPet = await Shelter.findByIdAndUpdate(
            shelterId,
            { $push: { pets: petID } },
            { new: true }
        );

        return res.status(200).json(addPet);

    } catch (error) {

        next(error);
        
    }
})

router.get('/', async ( req, res, next) => {
    
    try {
        // .populate('/pets') to merge and print the shelters with your pets information
        const allShelters = await Shelter.find().populate('pets');
        return res.status(200).json(allShelters);

    } catch (error) {

        next(error);
        
    }
})

module.exports = router;