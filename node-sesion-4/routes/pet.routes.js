const express = require('express');
const PET = require('../models/Pet');
const router = express.Router();


// Route to find all pets in the data base
router.get("/", async ( req, res ) => {

    try {
        console.log('hello');
        const allPets = await PET.find();
        
        return res.status(200).send(allPets);

    } catch (error) {

        return res.status(500).json(error);
    }
    

})

// Route to find a pet by the name in the data base.

router.get("/name/:name", async ( req, res ) => {
        
    const searchCapitalizer = (search) => {

        return search.charAt(0).toUpperCase() + search.slice(1);
    }
    const search  = searchCapitalizer(req.params.name);

    try {
 
        const petRequest = await PET.find({ name: search });

        return res.status(200).json(petRequest);

    } catch (error) {
        return res.status(500),json(error);
    }
})

// Route POST to add pets, use postMan app to create a new PEt.

router.post('/create', async (req, res, next) => {
    
    // create a new pet instace 
    try {
        const newPet = new PET ({
            name: req.body.name,
            age: req.body.age, 
            species: req.body.species,
        });
    // save the pet in the dataBase
        const createPet = await newPet.save();

        return res.status(200).json(createPet);

    } catch (err) {

    // We Pass the err to the next function so that node handles the error. 
        next(err);
    }

})

// Route put to modify a pet by id

router.put("/edit", async (req, res, next) => {

    try {

        const id = req.body.id;
        const updatePet = await PET.findByIdAndUpdate(

            // A id to find and update de document
            id,
            // keys to edit
            {
                name: req.body.name,
                age: req.body.age,
                species: req.body.species,
            },
            // This option update the document
            {
                new: true
            }
        );
        return res.status(200).json(updatePet);

    } catch (err) {
        
        next(err)
        
    }
})

// Router Delete to delete a pet by id

router.delete('/delete', async ( req, res, next) => {

    try {
        
        const id = req.params.id;
        const deletePet = await PET.findByIdAndDelete(id);

        return res.status(200).json('Pet deleted successfully');
                
    } catch (error) {

        next(error);
        
    }
})

module.exports = router;