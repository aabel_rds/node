const mongoose = require('mongoose');

const schema = mongoose.Schema;

const shelterSchema = new schema(
    {
        name: { type: String, required: true },
        location : { type: String, required: true},

        // Mongoose type and Pet model reference
        pets: [{ type: mongoose.Types.ObjectId, ref: 'Pet'}],
    },
    {
        timestamps: true,
    });

    const Shelter = mongoose.model('Shelter', shelterSchema);

    module.exports = Shelter; 