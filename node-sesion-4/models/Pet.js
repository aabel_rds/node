const mongoose = require('mongoose');

const schema = mongoose.Schema;

const petSchema = new schema(
    {
        name: { type: String, required: true},
        age: { type: Number, required: false},
        species: { type: String, required: true},
    }, 
    {
        timestamps: true,
    }
);

const Pet = mongoose.model('Pet', petSchema);
module.exports = Pet;
