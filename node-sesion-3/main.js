const express = require('express');
require('./db.js');
const petRoutes = require('./routes/pet.routes');

const PORT = 5000;
const server = express();

// server.use(express.json());
// server.use(express.urlencoded({ extended: false }));

// Pets Routes
server.use("/pets", petRoutes);


// Middelware, controlling routes errors (express)
server.use('*', ( req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})
// Controlling routes errors using express and the 4th parameter error 
server.use((err, req, res, next) =>{
    
    return res.status( err.status || 500).json( err.message || 'Unexpected Error');
});
// Server started
server.listen(PORT, () => console.log(`Server listening on ${PORT}`));