const express = require('express');
const PET = require('../models/Pet');
const router = express.Router();


// Route to find all pets in the data base
router.get("/", async ( req, res ) => {

    try {
        console.log('hello');
        const allPets = await PET.find();
        
        return res.status(200).send(allPets);

    } catch (error) {

        return res.status(500).json(error);
    }
    

})

// Route to find a pet by the name in the data base.

router.get("/name/:name", async ( req, res ) => {
        
    const searchCapitalizer = (search) => {

        return search.charAt(0).toUpperCase() + search.slice(1);
    }
    const search  = searchCapitalizer(req.params.name);

    try {
 
        const petRequest = await PET.find({ name: search });

        return res.status(200).json(petRequest);

    } catch (error) {
        return res.status(500),json(error);
    }
})

module.exports = router;